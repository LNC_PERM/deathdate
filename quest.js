(() => {
  const container = document.querySelector('.main__container');
  const header = document.querySelector('.header__container');
  let currentQuestion = 0;
  let age = 18;

  const questions = [
    {
      q: "Когда Вы чувствуете себя наиболее комфортно?",
      comment: 'Мы расскажем Вам не только подробности вашей смерти, но также поможем Вам избежать этой ужасной даты и продлить вашу жизнь на многие годы.',
      buttons: [{ text: "Утро", val: 1 }, { text: "День", val: 2 }, { text: "Вечер", val: 3 }, { text: "Ночь", val: 4 }],
      action: nextQuestion
    },
    {
      q: "Укажите свою дату рождения:",
      comment: 'Уже совсем скоро Вы узнаете много интересного о своем будущем!',
      selectors: [{ from: 1, to: 31, text: 'День', id: 'bday' }, { from: 1, to: 12, text: 'Месяц', id: 'bmon' }, { from: 1950, to: 2003, text: 'Год', id: 'byear' }],
      buttons: [{ text: "Далее", val: 3 }],
      action: () => {
        if (BDateOk()) {
          age = getAge();
          nextQuestion()
        }
      }
    },
    {
      q: "Снятся ли Вам умершие люди?",
      comment: 'Смерть родного человека – одно из тяжелейших испытаний в жизни каждого из нас!',
      buttons: [{ text: "Да", val: 1 }, { text: "Нет", val: 2 }, { text: "Иногда", val: 3 }],
      action: nextQuestion
    },
    {
      q: "Запись, которую Вы услышите, может шокировать людей с неокрепшей психикой. Вы готовы узнать, что ждет именно Вас?",
      comment: () => {
        if (age <= 35) {
          return 'По вам скучает очень близкий человек, которого больше нет в мире живых.'
        } else if (age >= 36 && age <= 45) {
          return 'По вам скучает очень близкий человек, которого больше нет в мире живых. Возможно это дедушка или бабушка.'
        } else if (age > 45) {
          return 'По вам скучает очень близкий человек, которого больше нет в мире живых. Возможно это кто-то из Ваших родителей.'
        }
      },
      message: true,
      onset: true,
      buttons: [{ text: "Да", val: 1 }, { text: "Затрудняюсь ответить", val: 2 }],
      action: recordingMsg
    },
  ];

  function nextQuestion() {
    n = currentQuestion;
    container.innerHTML = '';
    const qText = document.createElement('p');
    qText.innerText = questions[n].q;
    qText.classList.add('question');
    container.append(qText);
    header.innerText = typeof (questions[n].comment) === 'function' ? questions[n].comment() : questions[n].comment;
    header.classList.remove(...['message', 'onset']);
    questions[n].message && header.classList.add('message');
    questions[n].onset && setTimeout(() => { header.classList.add('onset'); }, 200);
    questions[n].selectors && questions[n].selectors.length
      && questions[n].selectors.forEach(el => {
        container.append(selector(el));
      });
    questions[n].buttons.forEach(el => {
      const btn = document.createElement('button');
      btn.innerText = el.text;
      btn.classList.add('btn', 'quest__btn');
      container.append(btn);
      btn.addEventListener('click', questions[n].action);
    });
    const smallText = document.createElement('small');
    smallText.innerText = `Вопрос ${(n + 2)} - 5`;
    smallText.classList.add('gray-text', 'quest__bottom-text');
    container.append(smallText);

    currentQuestion++;
  }

  function selector(s) {
    const select = document.createElement('select');
    select.id = s.id;
    select.classList.add('select');
    const option = document.createElement('option');
    option.innerText = s.text;
    option.value = 0;
    option.disabled = true;
    option.selected = true;
    select.append(option);
    for (let i = s.from; i <= s.to; i++) {
      const option = document.createElement('option');
      option.value = i;
      option.innerText = i < 100 ? ('0' + i).slice(-2) : i;
      select.append(option);
    }
    return select;
  }

  function BDateOk() {
    let result = true;
    const bDate = [
      document.getElementById('bday'),
      document.getElementById('bmon'),
      document.getElementById('byear')
    ];
    bDate.forEach(el => {
      el.classList.remove('select-alert');
      if (!parseInt(el.value)) {
        el.classList.add('select-alert');
        result = false;
      }
    });
    return result;
  }

  function getAge() {
    let bday = parseInt(document.getElementById('bday').value);
    let bmon = parseInt(document.getElementById('bmon').value);
    let byear = parseInt(document.getElementById('byear').value);
    const now = new Date();
    return now.getFullYear() - byear - ((100 * (now.getMonth() + 1) + now.getDate() >= 100 * bmon + bday) ? 0 : 1);
  }

  function recordingMsg() {
    header.innerHTML = '';
    header.classList.remove('message');
    document.querySelector('.header').classList.remove('header_lined');
    container.innerHTML = '';
    container.classList.remove(...['quest', 'main__container']);
    container.classList.add('recording');
    const indicator = document.createElement('div');
    indicator.classList.add('indicator');
    container.append(indicator);

    const viewCounter = document.createElement('div');
    viewCounter.classList.add(...['count-progress', 'gray-text']);
    container.append(viewCounter);

    const smallText = document.createElement('small');
    smallText.innerText = `Запись сообщения`;
    smallText.classList.add('gray-text', 'quest__bottom-text');
    container.append(smallText);

    let counter = 0;
    let interval = setInterval(() => {
      counter += 10;
      if (counter > 100) {
        clearInterval(interval);
        showSummary();
      } else {
        viewCounter.innerText = `${counter}%`;
        indicator.style.backgroundSize = `${counter}%`;
      }
    }, 250);
  }

  function showSummary() {
    header.innerHTML = '';
    container.innerHTML = '';
    container.classList.remove('recording');
    header.classList.add(...['message','meassge_small']);
    header.classList.remove('comment');
    header.style.opacity = 1;
    document.querySelector('.header').style.marginBottom = 0;
    header.innerHTML = 'Спасибо за Ваши ответы! <b>Мы подготовили для Вас персональную аудио запись с вашим прогнозом.</b>';
    
    const p1 = document.createElement('p');
    p1.classList.add('summary-texts');
    p1.innerText = 'Вы можете узнать, как повлиять на события, которые ожидают вас в ближайшем будущем.';
    container.append(p1);

    const tomorrow = tomorrowStr();
    const pAlert = document.createElement('p');
    pAlert.classList.add(...['summary-main','highlight']);
    pAlert.innerHTML = `<b class="caps">Первое значимое событие может произойти уже ${tomorrow}</b>, Вам надо быть готовым, что бы последствия не оказались необратимыми.`;
    container.append(pAlert);

    const p2 = document.createElement('p');
    p2.classList.add('summary-texts');
    p2.innerText = 'Нажмите на кнопку ниже прямо сейчас и наберите наш номер телефона. Прослушайте важную информацию!';
    container.append(p2);

    const btn = document.createElement('button');
    btn.innerText = 'Позвонить и прослушать';
    btn.classList.add(...['btn', 'green-btn']);
    btn.addEventListener('click', async ()=>{await callAndListen();});
    container.append(btn);

  }

  function tomorrowStr() {
    const now = new Date();
    const tomorrow = new Date(now.getTime() + (24 * 60 * 60 * 1000));
    return ('0' + tomorrow.getDate()).slice(-2) + '.' + ('0' + (tomorrow.getMonth() + 1)).slice(-2) + '.' + tomorrow.getFullYear();
  }

  async function callAndListen(){
    showLoader();
    const response = await fetch(`https://swapi.dev/api/people/1/`);
    const data = await response.json();
    showResultTable(data);
  }

  function showLoader(){
    header.innerHTML = '';
    header.classList.remove('message');
    container.innerHTML = '';
    const loader = document.createElement('div');
    loader.classList.add('loader');
    container.append(loader);
  }

  function showResultTable(data){
    container.innerHTML = '';
    header.classList.add('message');
    header.classList.remove('comment');
    header.innerText = 'Ваше сообщение:';
    const table = document.createElement('table');
    table.classList.add('table');
    const tbody = document.createElement('tbody');

    for(key in data){
      const tr = document.createElement('tr');
      let td = document.createElement('td');
      td.innerText = key;
      tr.append(td);
      td = document.createElement('td');
      td.innerText = Array.isArray(data[key]) ? data[key].join(', ') : data[key];
      tr.append(td);
      tbody.append(tr);
    }
    table.append(tbody);
    container.append(table);
  }

  document.addEventListener("DOMContentLoaded", () => {
    nextQuestion(0);
  });

})();